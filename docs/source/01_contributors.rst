.. _contributors:

Contributing organisations
============
.. |copy| unicode:: 0xA9 .. copyright sign

.. |logoGHRSST| image:: ../../img/GHRSST_logo.png
   :height: 50pt
   :align: middle
   :alt: GHRSST logo
   :target: https://www.ghrsst.org/

.. |logoCOP| image:: ../../img/COP_logo.png
   :height: 50pt
   :align: middle
   :alt: Copernicus logo
   :target: https://www.copernicus.eu/en

.. |logoESA| image:: ../../img/ESA_logo.png
   :height: 50pt
   :align: middle
   :alt: ESA logo
   :target: https://www.esa.int/

.. |logoEUMETSAT| image:: ../../img/EUMETSAT_logo.png
   :height: 50pt
   :align: middle
   :alt: EUMETSAT logo
   :target: https://www.eumetsat.int/

.. |logoNASA| image:: ../../img/NASA_logo.png
   :height: 50pt
   :align: middle
   :alt: NASA logo
   :target: https://www.nasa.gov/

.. |logoNOAA| image:: ../../img/NOAA_logo.png
   :height: 50pt
   :align: middle
   :alt: NOAA logo
   :target: https://www.noaa.gov/

.. list-table::
   :widths: 50 50
   :header-rows: 0

   * - |logoGHRSST|
     - Copyright |copy| GHRSST
   * - |logoCOP|
     - Copyright |copy| European Union
   * - |logoESA|
     - Copyright |copy| ESA
   * - |logoEUMETSAT|
     - Copyright |copy| EUMETSAT
   * - |logoNASA|
     - Copyright |copy| NASA
   * - |logoNOAA|
     - Copyright |copy| NOAA

Contributing authors
============
* Ben Loveday
* ...