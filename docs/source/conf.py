# Configuration file for the Sphinx documentation builder.

# -- Project information

project = 'Fundamentals of sea surface temperature'
org = f'{project} consortium'
author = f'{org} and contributors'
copyright = f'{org} - CC BY-SA 4.0 (2025)'
version = '0.0.1'
release = version

# -- General configuration

extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.intersphinx',
    'sphinxcontrib.youtube'
]

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output
html_theme = 'furo'

# -- Options for EPUB output
epub_show_urls = 'footnote'
