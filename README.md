# fundamentals-of-sea-surface-temperature

<hr>

[![Python](https://img.shields.io/badge/python%203.10-anaconda-green)](https://www.anaconda.com/products/distribution)
[![License: CC-BY-4.0](https://img.shields.io/badge/License-CC_BY_SA_4.0-green.svg)](LICENSE.txt)

<hr>

![image](../img/FoSST_banner.png)

## Overview

This repository contains the source documentation and configuration files need to build the **fundamentals-of-sea-surface-temperature** community user guide. The documentation is built at [fundamentals-of-ocean-colour.readthedocs.io](https://fundamentals-of-sea-surface-temperature.readthedocs.io/en/latest/).

## Ownership

This software and all associated intellectual property rights (IPRs) are owned by xxxxxx.

## License

This documentation is licensed under CC-BY-4.0. See file LICENSE.txt for details on 
the usage and distribution terms. No dependencies are distributed as part of this 
package. Copyright 2025 xxxxxxxxxxxx.

All product names, logos, and brands are property of their respective owners. 
All company, product and service names used in this website are for identification 
purposes only.

## Authors

* [**Ben Loveday**](mailto://ops@eumetsat.int) - [Innoflair/EUMETSAT](http://www.eumetsat.int)

Please see the AUTHORS.txt file for more information on contributors.

## Collaborating, contributing, questions and issues

For all queries on this documentation, please raise an issue at [fundamentals-of-sea-surface-temperature](https://gitlab.com/benloveday/fundamentals-of-sea-surface-temperature).

<hr>